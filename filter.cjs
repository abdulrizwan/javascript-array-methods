// function filter(elements, cb) {
//     // Do NOT use .filter, to complete this function.
//     // Similar to `find` but you will return an array of all elements that passed the truth test
//     // Return an empty array if no elements pass the truth test
// }



let filter = (elements, cb) => {
    if(!Array.isArray(elements) && typeof cd != 'function'){
        return []
    }
    let filteredArr = []
    for(let index = 0; index < elements.length; index++){
        if(cb(elements[index], index, elements) == true){
            filteredArr.push(elements[index])
        }
    }
    return filteredArr
}

module.exports = filter
