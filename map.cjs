
let map = (elements, cb) => {
    if(!Array.isArray(elements) && typeof cb != 'function'){
        return []
    }
    let mappedArr = []
    for(let index = 0; index < elements.length; index++){
        mappedArr.push(cb(elements[index], index, elements))
    }
    return mappedArr
}

module.exports = map
