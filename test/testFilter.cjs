const filter = require('../filter.cjs')

let cb = (element, index, elements) => {
    if(typeof element === 'number'){
        return element%2 === 0
    }
    if(element.length > 4){
        return true
    }
    return false
}

let arr = [2, 4, 5, 7, 90]

console.log(filter(arr, cb))
