// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

// function flatten(elements) {
//     // Flattens a nested array (the nesting can be to any depth).
//     // Hint: You can solve this using recursion.
//     // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }



let flatten = (elements, depth = 1) => {
    let flattenArr = []
    let currDepth = 0;
    for(let index = 0; index < elements.length; index++){
        if(Array.isArray(elements[index]) && currDepth < depth){
            flattenArr = flattenArr.concat(flatten(elements[index], depth-1))
            flatten(elements[index])
        }
        else{
            if(elements[index] != undefined){
                flattenArr.push(elements[index])
            }
        }
    }
    return flattenArr
}

module.exports = flatten

